from myhdl import block, always_seq, Signal, intbv, enum, instances, ConcatSignal

@block
def filter_(clock, reset, in_sig, out_sig, in_len=3, out_len=6, N=5, data_size=8, out_size=24):


    @always_seq(clock.posedge, reset=reset)
    def seq():
        #rozbicie shadow signala in_sig na liste
        f = [0] * in_len
        for i in range(in_len):
            u2 = int(in_sig[data_size * (i + 1): data_size * i])
            if u2 >= 2**(data_size-1):
                u2 -= 2**data_size
            f[in_len - 1 - i] = u2
        #print('f: ', f)

        #wybor filtru w zaleznosci od wybranego wspolczynnika nadprobkowania
        assert (N==2) or (N==3) or (N==4) or (N==5) or (N==10), "niepoprawny wspolczynnik nadprobkowania"
        if N == 2:
            g = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 1, 0, -2, 0, 3, 0, -4, 0, 5, 0, -7, 0, 9, 0, -11, 0, 14, 0, -18, 0,
             23, 0, -29, 0, 37, 0, -47, 0, 62, 0, -84, 0, 122, 0, -209, 0, 636, 1000, 636, 0, -209, 0, 122, 0, -84, 0,
             62, 0, -47, 0, 37, 0, -29, 0, 23, 0, -18, 0, 14, 0, -11, 0, 9, 0, -7, 0, 5, 0, -4, 0, 3, 0, -2, 0, 1, 0,
             -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elif N == 3:
            g = [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 1, 2, 0, -2, -3, 0, 4, 5, 0, -6, -7, 0, 10, 11, 0, -15, -17, 0,
                 21, 24, 0, -30, -34, 0, 43, 48, 0, -62, -70, 0, 93, 109, 0, -159, -201, 0, 411, 826, 1000, 826, 411, 0,
                 -201, -159, 0, 109, 93, 0, -70, -62, 0, 48, 43, 0, -34, -30, 0, 24, 21, 0, -17, -15, 0, 11, 10, 0, -7,
                 -6, 0, 5, 4, 0, -3, -2, 0, 2, 1, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elif N == 4:
            g = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, -1, -2, -2, 0, 3, 4, 4, 0, -5, -9, -7, 0, 9, 15, 12, 0, -16, -25, -20,
                 0, 26, 41, 33, 0, -41, -66, -52, 0, 67, 108, 88, 0, -119, -200, -173, 0, 296, 633, 899, 1000, 899, 633,
                 296, 0, -173, -200, -119, 0, 88, 108, 67, 0, -52, -66, -41, 0, 33, 41, 26, 0, -20, -25, -16, 0, 12, 15,
                 9, 0, -7, -9, -5, 0, 4, 4, 3, 0, -2, -2, -1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]
        elif N == 5:
            g = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, -1, -2, -3, -2, 0, 3, 6, 7, 5, 0, -7, -14, -16, -11, 0, 15, 27, 30,
                 21, 0, -27, -49, -55, -38, 0, 48, 88, 100, 70, 0, -91, -170, -200, -147, 0, 228, 497, 752, 934, 1000,
                 934, 752, 497, 228, 0, -147, -200, -170, -91, 0, 70, 100, 88, 48, 0, -38, -55, -49, -27, 0, 21, 30, 27,
                 15, 0, -11, -16, -14, -7, 0, 5, 7, 6, 3, 0, -2, -3, -2, -1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]
        elif N == 10:
            g = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, -1, -3, -5, -7, -9, -11, -11, -9, -6, 0, 8, 17, 27, 36, 43, 47, 45,
                 37, 22, 0, -28, -60, -94, -124, -146, -156, -150, -123, -73, 0, 96, 211, 340, 476, 611, 737, 846, 929,
                 982, 1000, 982, 929, 846, 737, 611, 476, 340, 211, 96, 0, -73, -123, -150, -156, -146, -124, -94, -60,
                 -28, 0, 22, 37, 45, 47, 43, 36, 27, 17, 8, 0, -6, -9, -11, -11, -9, -7, -5, -3, -1, 0, 1, 1, 1, 1, 1,
                 0, 0, 0, 0, 0]

        #zwiekszanie ilosci probek
        oversampling = [0 if i % N else f[int(i / N)] for i in range(in_len * N)]

        #obliczanie splotu
        assert out_len == in_len*N + 101 - 1, "niepoprawna dlugosc wektora wyjsciowego conv_sig"
        y = [0] * out_len
        g_ = [0] * out_len #przekopiowany g z dorzuconymi zerami
        for i in range(101):
            g_[i] = g[i]
        for n in range(out_len):
            sum_ = 0
            for k in range(in_len*N):
                sum_ = sum_ + oversampling[k]*g_[n-k]
            y[n] = int(sum_ / 1000) #dzielenie ze wzgledu na zwiekszenie precyzji intow w liscie g (w sincu)
        #print('y: ', y)

        #generowanie signalu wyjsciowego
        y_tmp = [Signal(intbv(i)[out_size:]) for i in y]
        out_sig.next = ConcatSignal(*y_tmp)
        #print(ConcatSignal(*y_tmp))

    return instances()
