from myhdl import block, always_seq, Signal, intbv, enum, instances, ConcatSignal, always_comb

@block
def filter_(clock, reset, in_sig, out_sig, in_len=3, out_len=6, upsampling=5, data_size=24):
    y = [Signal(intbv(0)[data_size:].signed()) for _ in range(out_len)]
    out_sig_c = Signal(intbv()[data_size*out_len:])
    sum_ = Signal(intbv(0)[data_size:].signed())
    oversampling = [Signal(intbv(0)[data_size:].signed())] * in_len * upsampling
    y_end_flag = Signal(bool(0)) #gdy 1, koniec obliczania splotu
    g_vals = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, -1, -3, -5, -7, -9, -11, -11, -9, -6, 0, 8, 17, 27, 36, 43, 47, 45,
                     37, 22, 0, -28, -60, -94, -124, -146, -156, -150, -123, -73, 0, 96, 211, 340, 476, 611, 737, 846, 929,
                     982, 1000, 982, 929, 846, 737, 611, 476, 340, 211, 96, 0, -73, -123, -150, -156, -146, -124, -94, -60,
                     -28, 0, 22, 37, 45, 47, 43, 36, 27, 17, 8, 0, -6, -9, -11, -11, -9, -7, -5, -3, -1, 0, 1, 1, 1, 1, 1,
                     0, 0, 0, 0, 0]

    n = Signal(intbv(0)[data_size:])
    k = Signal(intbv(0)[data_size:])

    f = [Signal(intbv(0)[data_size:].signed())] * in_len
    g_ = [Signal(intbv(0)[data_size:].signed())] * out_len

    @always_seq(clock.posedge, reset=reset)
    def seq():
        #rozbicie shadow signala in_sig na liste
        for i in range(in_len):
            f[in_len - 1 - i] = in_sig[data_size * (i + 1): data_size * i] #odbijanie na potrzeby splotu
        print('f: ', f)

        #wybor filtru w zaleznosci od wybranego wspolczynnika nadprobkowania
        assert (upsampling == 2) or (upsampling == 3) or (upsampling == 4) or (upsampling == 5) or (upsampling == 10), "niepoprawny wspolczynnik nadprobkowania"

        #tworzenie tabeli z filtrem odpowiednio wydluzonej zerami
        filter_end_flag = 0  # gdy 1, koniec przepisywania wartosci filtrow ze stalej
        if filter_end_flag == 0:
            filter_end_flag = 1

            if upsampling == 2:
                for i in range(in_len+1):
                    g_[i].next = g_vals[i]
                # g_.next = [Signal(intbv(i)[data_size:].signed()) for i in [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 1, 0, -2, 0, 3, 0, -4, 0, 5, 0, -7, 0, 9, 0, -11, 0, 14, 0, -18, 0, 23, 0, -29, 0, 37, 0, -47, 0, 62, 0, -84, 0, 122, 0, -209, 0, 636, 1000, 636, 0, -209, 0, 122, 0, -84, 0, 62, 0, -47, 0, 37, 0, -29, 0, 23, 0, -18, 0, 14, 0, -11, 0, 9, 0, -7, 0, 5, 0, -4, 0, 3, 0, -2, 0, 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
            # elif upsampling == 3:
            #     g_.next = [Signal(intbv(i)[data_size:].signed()) for i in
            #          [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 1, 2, 0, -2, -3, 0, 4, 5, 0, -6, -7, 0, 10, 11, 0, -15, -17, 0,
            #          21, 24, 0, -30, -34, 0, 43, 48, 0, -62, -70, 0, 93, 109, 0, -159, -201, 0, 411, 826, 1000, 826, 411, 0,
            #          -201, -159, 0, 109, 93, 0, -70, -62, 0, 48, 43, 0, -34, -30, 0, 24, 21, 0, -17, -15, 0, 11, 10, 0, -7,
            #          -6, 0, 5, 4, 0, -3, -2, 0, 2, 1, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
            # elif upsampling == 4:
            #     g_.next = [Signal(intbv(i)[data_size:].signed()) for i in
            #          [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, -1, -2, -2, 0, 3, 4, 4, 0, -5, -9, -7, 0, 9, 15, 12, 0, -16, -25, -20,
            #          0, 26, 41, 33, 0, -41, -66, -52, 0, 67, 108, 88, 0, -119, -200, -173, 0, 296, 633, 899, 1000, 899, 633,
            #          296, 0, -173, -200, -119, 0, 88, 108, 67, 0, -52, -66, -41, 0, 33, 41, 26, 0, -20, -25, -16, 0, 12, 15,
            #          9, 0, -7, -9, -5, 0, 4, 4, 3, 0, -2, -2, -1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]]
            # elif upsampling == 5:
            #     g_.next = [Signal(intbv(i)[data_size:].signed()) for i in [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, -1, -2, -3, -2, 0, 3, 6, 7, 5, 0, -7, -14, -16, -11, 0, 15, 27, 30,
            #          21, 0, -27, -49, -55, -38, 0, 48, 88, 100, 70, 0, -91, -170, -200, -147, 0, 228, 497, 752, 934, 1000,
            #          934, 752, 497, 228, 0, -147, -200, -170, -91, 0, 70, 100, 88, 48, 0, -38, -55, -49, -27, 0, 21, 30, 27,
            #          15, 0, -11, -16, -14, -7, 0, 5, 7, 6, 3, 0, -2, -3, -2, -1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]]
            elif upsampling == 10:
                for i in range(in_len+1):
                    g_[i].next = g_vals[i]
                # g_.next = [Signal(intbv(i)[data_size:].signed()) for i in
                #      [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, -1, -3, -5, -7, -9, -11, -11, -9, -6, 0, 8, 17, 27, 36, 43, 47, 45,
                #      37, 22, 0, -28, -60, -94, -124, -146, -156, -150, -123, -73, 0, 96, 211, 340, 476, 611, 737, 846, 929,
                #      982, 1000, 982, 929, 846, 737, 611, 476, 340, 211, 96, 0, -73, -123, -150, -156, -146, -124, -94, -60,
                #      -28, 0, 22, 37, 45, 47, 43, 36, 27, 17, 8, 0, -6, -9, -11, -11, -9, -7, -5, -3, -1, 0, 1, 1, 1, 1, 1,
                #      0, 0, 0, 0, 0]]
            else:
                pass
        else:
            #zwiekszanie ilosci probek
            for i in range(in_len):
                oversampling[i * upsampling] = f[i // upsampling]

            #do obliczania splotu
            assert out_len == in_len * upsampling + 101 - 1, "niepoprawna dlugosc wektora wyjsciowego conv_sig"


            #obliczanie splotu

            if y_end_flag == 0:
                if k < in_len * upsampling:
                    sum_.next = sum_ + oversampling[k] * g_[n - k]
                    k.next = k + 1
                else:
                    y[n].next = sum_
                    sum_.next = 0
                    k.next = 0
                    if n == out_len:
                        y_end_flag.next = 1
                    else:
                        n.next = n + 1
            else:
                pass
                # print('y: ', y)

                # generowanie signalu wyjsciowego
                # out_sig.next = ConcatSignal(*y)
    out_sig_c = ConcatSignal(*y)

    @always_comb
    def map_out():
        out_sig.next = out_sig_c


    return instances()
