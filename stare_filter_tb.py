from myhdl import block, instance, Signal, ResetSignal, StopSimulation, intbv, delay, ConcatSignal, instances
from clk_stim import clk_stim
from stare_filter import filter_
import matplotlib.pyplot as plt

@block
def test_filter():
    #konfiguracja
    data_size = 32
    out_size = 64
    f = [0,159,95,-5,59,100,-59,-195,-95,41,0,-41,95,195,59,-100,-59,5,-95,-159,-0,159,95,-5,59,100,-59,-195,-95,41,0,-41,95,195,59,-100,-59,5,-95,-159,-0,159,95,-5,59,100,-59,-195,-95,41,-0,-41,95,195,59,-100,-59,5,-95,-159,-0,159,95,-5,59,100,-59,-195,-95,41,0,-41,95,195,59,-100,-59,5,-95,-159,-0,159,95,-5,59,100,-59,-195,-95,41,-0,-41,95,195,59,-100,-59,5,-95,-159]
    N = 10

    f_len = len(f)
    out_len = f_len*N + 101 - 1
    in_sig = ConcatSignal(*[Signal(intbv(i)[data_size:]) for i in f])
    out_sig = Signal(intbv(0)[out_size * out_len:])
    clk = Signal(bool(0))
    reset = ResetSignal(0, active=0, async=False)

    clk_gen = clk_stim(clk, period=10)

    uut = filter_(clk, reset, in_sig, out_sig, in_len=f_len, out_len=out_len,
                 N=N, data_size=data_size, out_size=out_size)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(50)
        yield clk.negedge
        reset.next = 1

    @instance
    def final():
        yield delay(100)
        print('in_sig: ', in_sig)
        print('out_sig: ', out_sig)

        out = [0] * out_len
        for i in range(out_len):

            u2 = int(out_sig[out_size * (i + 1): out_size * i])
            if u2 >= 2**(out_size-1):
                u2 -= 2**out_size
            out[out_len - 1 - i] = u2
        print("in: ", f)
        print("out: ", out)

        plt.figure(1)
        plt.plot(f)

        plt.figure(2)
        plt.plot(out)
        plt.show()

    uut.convert(hdl='VHDL')

    return instances()


tb = test_filter()
tb.config_sim(trace=True)
tb.run_sim(duration=200)
print('bye')
